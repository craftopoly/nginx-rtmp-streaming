# nginx RTMP video streaming

[![Ubuntu](https://img.shields.io/badge/Ubuntu-16.04-green.svg)](http://releases.ubuntu.com/16.04/)
[![nginx](https://img.shields.io/badge/nginx-1.12.0-green.svg)](https://nginx.org/en/CHANGES-1.12)
[![nginx-rtmp-module](https://img.shields.io/badge/nginx_rtmp_module-1.1.11-green.svg)](https://github.com/arut/nginx-rtmp-module/releases/tag/v1.1.11)

## Installation

Simply clone this repo and run the install script:

```bash
git clone https://craftopoly@gitlab.com/craftopoly/nginx-rtmp-streaming.git
cd nginx-rtmp-streaming
chmod u+x install
./install
```

Start and check that nginx server is running correctly:

```bash
sudo service nginx start
sudo service nginx status
```

Enjoy 🔥
